package ssh

import (
	"io"
	"os"
	"os/exec"
	"strings"

	"golang.org/x/crypto/ssh"
)

func init() {
	if len(os.Args) > 1 && os.Args[1] == "__rsync-helper__" {
		args := strings.Join(os.Args[3:], " ")

		childReadPipe := os.NewFile(3, "pipe")
		childWritePipe := os.NewFile(4, "pipe")
		childWritePipe.Write([]byte(args + "\n"))

		go io.Copy(os.Stdout, childReadPipe)
		io.Copy(childWritePipe, os.Stdin)
		os.Exit(0)
	}
}

// Rsync runs rsync over an ssh.Client
//
// This is particularly difficult because rsync usually calls ssh directly. We
// work around this by creating a pair of unix pipes and forking those pipes to
// rsync. rsync then forks to another program (typically ssh) which inherits the
// same pipes. We instruct rsync to call it's parent binary with the special
// option `__rsync-helper__`. When this option is seen in `init()`, the program
// ignores all it's other duties and copies `fd(4) -> fd(0)` and `fd(1) ->
// fd(4)`
func Rsync(client *ssh.Client, src string, dst string, direction Direction, options ...string) error {
	session, err := client.NewSession()
	if err != nil {
		return err
	}

	childReadPipe, parentWritePipe, err := os.Pipe()
	if err != nil {
		panic(err)
	}

	parentReadPipe, childWritePipe, err := os.Pipe()
	if err != nil {
		panic(err)
	}

	helperPath := os.Args[0] + " __rsync-helper__"
	if direction == LocalToRemote {
		dst = ":" + dst
	} else {
		src = ":" + src
	}

	args := []string{
		"-e",
		helperPath,
	}
	args = append(args, options...)
	args = append(args, src, dst)
	localRsync := exec.Command("rsync", args...)

	localRsync.ExtraFiles = []*os.File{
		childReadPipe,
		childWritePipe,
	}
	localRsync.Stdin = os.Stdin
	localRsync.Stdout = os.Stdout
	localRsync.Stderr = os.Stderr
	if err := localRsync.Start(); err != nil {
		panic(err)
	}
	childReadPipe.Close()
	childWritePipe.Close()
	defer parentReadPipe.Close()
	defer parentWritePipe.Close()
	defer localRsync.Process.Kill()

	sshStdin, err := session.StdinPipe()
	if err != nil {
		return err
	}

	sshStdout, err := session.StdoutPipe()
	if err != nil {
		return err
	}

	err = session.Start("bash")
	if err != nil {
		return err
	}

	go io.Copy(sshStdin, parentReadPipe)
	io.Copy(parentWritePipe, sshStdout)

	return nil
}
