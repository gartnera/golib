package ssh

import (
	"errors"
	"fmt"
	"net/url"
	"os/user"
	"strings"

	"golang.org/x/crypto/ssh"
)

func DialURLEnv(u string) (*ssh.Client, error) {
	config := MakeSSHConfigEnv()
	return DialURL(u, config)
}

// DialURL dials an SSH server via URL
func DialURL(u string, config *ssh.ClientConfig) (*ssh.Client, error) {
	if !strings.HasPrefix(u, "ssh://") {
		u = "ssh://" + u
	}

	url, err := url.Parse(u)
	if err != nil {
		return nil, err
	}
	userName := url.User.Username()
	if userName == "" {
		user, err := user.Current()
		if err != nil {
			return nil, err
		}
		userName = user.Username
	}
	config.User = userName

	port := url.Port()
	if port == "" {
		port = "22"
	}

	hostname := url.Host
	if hostname == "" {
		return nil, errors.New("hostname is required")
	}

	addr := fmt.Sprintf("%s:%s", hostname, port)

	return ssh.Dial("tcp", addr, config)
}
