package ssh

import (
	"bufio"
	"fmt"
	"net"
	"testing"
	"time"

	gDocker "gitlab.com/gartnera/golib/docker"
	gError "gitlab.com/gartnera/golib/error"
	gTest "gitlab.com/gartnera/golib/test"
	"golang.org/x/crypto/ssh"
)

func TestForward(t *testing.T) {
	err := gDocker.PullContainer(testingSSHFixture)
	gError.PanicError(err)

	t.Run("TCP->TCP", func(t *testing.T) {
		t.Parallel()
		id, ports, err := gDocker.StartContainerWithAllPorts(testingSSHFixture, false)
		defer gDocker.CleanupContainer(id)
		if err != nil {
			t.Error(err)
			return
		}

		target := fmt.Sprintf("%s:%s", testHost, ports["22"])

		cbConfig := KeyCallbackConfig{
			IgnoreHostKey: true,
		}

		config := &ssh.ClientConfig{
			User: "test",
			Auth: []ssh.AuthMethod{
				getSSHKey(t),
			},
			HostKeyCallback: KeyCallback(cbConfig),
		}

		client, err := ssh.Dial("tcp", target, config)
		if err != nil {
			t.Error(err)
			return
		}
		defer client.Close()

		session, err := StartInPTY(client, "socat TCP-LISTEN:1234,fork SYSTEM:'xargs -n1 echo'")
		if err != nil {
			t.Error(err)
			return
		}
		defer session.Close()

		forward := PortForward{
			LocalProto:  "tcp",
			LocalAddr:   "127.0.0.1:0",
			RemoteProto: "tcp",
			RemoteAddr:  "127.0.0.1:1234",
			SSHClient:   client,
		}

		addr, err := forward.Start()
		if err != nil {
			t.Error(err)
			return
		}
		defer forward.Stop()
		forward.WaitForRemote()

		conn, err := net.Dial("tcp", addr)
		if err != nil {
			t.Error(err)
			return
		}
		defer conn.Close()

		testToken := "1234test5678\n"
		conn.Write([]byte(testToken))

		conn.SetReadDeadline(time.Now().Add(time.Second))
		reader := bufio.NewReader(conn)
		res, err := reader.ReadString('\n')
		if err != nil {
			t.Error(err)
			return
		}

		gTest.Expect(t, testToken, res)
	})
}
