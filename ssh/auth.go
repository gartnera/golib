package ssh

import (
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
	"golang.org/x/crypto/ssh/knownhosts"

	gError "gitlab.com/gartnera/golib/error"
	gFile "gitlab.com/gartnera/golib/file"

	log "gitlab.com/gartnera/golib/log"
)

// PublicKeyFileAuthEnv Uses environment variables to call PublicKeyFileAuth
func PublicKeyFileAuthEnv() ssh.AuthMethod {
	file, _ := os.LookupEnv("SSH_KEY")
	if !gFile.Exists(file) {
		file = os.ExpandEnv("$HOME/.ssh/id_rsa")
	}
	log.Info("id_rsa set:", "file", file)
	return PublicKeyFileAuth(file)
}

// PublicKeyFileAuth Reads file to create ssh.AuthMethod
func PublicKeyFileAuth(file string) ssh.AuthMethod {
	buffer, err := ioutil.ReadFile(file)
	if err != nil {
		log.Info("Failed to load key " + err.Error())
		return nil
	}

	key, err := ssh.ParsePrivateKey(buffer)
	if err != nil {
		log.Info("Failed to parse key " + err.Error())
		return nil
	}
	return ssh.PublicKeys(key)
}

// AgentAuthEnv Gets the SSH agent via environment variable
func AgentAuthEnv() ssh.AuthMethod {
	socket := os.Getenv("SSH_AUTH_SOCK")
	return AgentAuth(socket)
}

// AgentAuth Connects to an agent socket
func AgentAuth(path string) ssh.AuthMethod {
	var err error
	var sshAgent net.Conn
	if sshAgent, err = net.Dial("unix", path); err == nil {
		log.Debug("Connected to auth sock", "path", path)
		return ssh.PublicKeysCallback(agent.NewClient(sshAgent).Signers)
	}
	log.Info("Failed to connect to auth sock " + err.Error())
	return nil
}

const (
	warningRemoteHostChanged = `@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the %v key sent by the remote host is
%v.
Please contact your system administrator.
Add correct host key in %v to get rid of this message.
Host key verification failed.
`
	promptToTrustHost = `The authenticity of host '%v' can't be established.
%v key fingerprint is %v
Are you sure you want to continue connecting (yes/no)? `
)

// AskToTrustHost prompts the user to trust a new key fingerprint while connecting to a host
func AskToTrustHost(addr, algo, fingerprint string, in io.Reader, out io.Writer) bool {
	var ans string

	fmt.Fprintf(out, promptToTrustHost, addr, algo, fingerprint)
	fmt.Fscanf(in, "%s\n", &ans)

	ans = strings.ToLower(ans)
	if ans != "yes" && ans != "y" {
		return false
	}

	return true
}

// KeyCallbackConfig configures KeyCallback
type KeyCallbackConfig struct {
	IgnoreHostKey   bool
	HostsFile       string
	ShouldAutoTrust bool
	SkipTrust       bool
	Input           io.Reader
	Output          io.Writer
}

// KeyCallbackEnv loads a KeyCallbackConfig from the environment and calls KeyCallback
func KeyCallbackEnv() ssh.HostKeyCallback {
	_, ignoreHostKey := os.LookupEnv("SSH_IGNORE_HOSTKEY")
	hostsFile, _ := os.LookupEnv("SSH_HOSTS_FILE")
	_, shouldAutoTrust := os.LookupEnv("SSH_AUTO_TRUST")
	_, skipTrust := os.LookupEnv("SSH_SKIP_TRUST")
	config := KeyCallbackConfig{
		IgnoreHostKey:   ignoreHostKey,
		HostsFile:       hostsFile,
		ShouldAutoTrust: shouldAutoTrust,
		SkipTrust:       skipTrust,
	}

	return KeyCallback(config)
}

// KeyCallback Reads from .ssh/known_hosts and/or prompts the user for trust
func KeyCallback(config KeyCallbackConfig) ssh.HostKeyCallback {
	input := config.Input
	output := config.Output
	if input == nil {
		input = os.Stdin
	}
	if output == nil {
		output = os.Stdout
	}

	if config.IgnoreHostKey {
		log.Info("Ignoring remote key")
		return ssh.InsecureIgnoreHostKey()
	}

	knownHostFile := config.HostsFile
	if knownHostFile == "" {
		knownHostFile = os.ExpandEnv("$HOME/.ssh/known_hosts")
	}
	log.Info("Set known hosts file", "file", knownHostFile)
	if !gFile.Exists(knownHostFile) {
		log.Warn("Creating known_hosts", "file", knownHostFile)
		err := os.MkdirAll(filepath.Dir(knownHostFile), 700)
		gError.PanicError(err)
		handle, err := os.Create(knownHostFile)
		gError.PanicError(err)
		handle.Close()
	}

	skipTrust := config.SkipTrust
	shouldAutoTrust := config.ShouldAutoTrust

	check, err := knownhosts.New(knownHostFile)
	if err != nil {
		log.WithError(err).Error("Error loading known hosts")
		return nil
	}
	return func(hostname string, remote net.Addr, key ssh.PublicKey) error {
		err := check(hostname, remote, key)
		if err != nil {
			keyErr := err.(*knownhosts.KeyError)
			matchingLen := len(keyErr.Want)
			fingerprint := ssh.FingerprintSHA256(key)
			if matchingLen != 0 {
				fmt.Printf(warningRemoteHostChanged, key.Type(), fingerprint, knownHostFile)
				return err
			}

			if skipTrust {
				log.Warn("Skipping trust", "hostname", hostname, "fingerprint", fingerprint)
				return nil
			}

			if shouldAutoTrust {
				log.Warn("Automatically trusting key", "hostname", hostname, "fingerprint", fingerprint)
			} else if !AskToTrustHost(hostname, key.Type(), fingerprint, input, output) {
				log.Info("User declined to trust host")
				return err
			}

			// TODO: write out IP address too
			normalizedHostname := knownhosts.Normalize(hostname)
			line := knownhosts.Line([]string{normalizedHostname}, key)
			err := gFile.Append(knownHostFile, line)
			if err != nil {
				log.Warn("Failed to append known hosts entry "+err.Error(), "file", knownHostFile, "line", line)
			}
		}
		return nil
	}
}

// MakeSSHConfigEnv  Returns an ssh.ClientConfig with our custom ssh.AuthMethod's and HostKeyCallbacks
func MakeSSHConfigEnv() *ssh.ClientConfig {
	methodsWithNils := []ssh.AuthMethod{
		AgentAuthEnv(),
		PublicKeyFileAuthEnv(),
	}
	methods := []ssh.AuthMethod{}
	for _, method := range methodsWithNils {
		if method != nil {
			methods = append(methods, method)
		}
	}
	return &ssh.ClientConfig{
		Auth:            methods,
		HostKeyCallback: KeyCallbackEnv(),
	}
}
