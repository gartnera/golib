package ssh

import (
	"context"
	"fmt"
	"io"
	"net"
	"sync"
	"time"

	gNet "gitlab.com/gartnera/golib/net"
	"golang.org/x/crypto/ssh"

	log "gitlab.com/gartnera/golib/log"
)

type Direction int

const (
	LocalToRemote Direction = iota
	RemoteToLocal           = iota
	SOCKS                   = iota
)

// PortForward allows you to easily forward ports over ssh.Client
type PortForward struct {
	LocalAddr   string
	LocalProto  string
	RemoteAddr  string
	RemoteProto string

	Direction Direction
	SSHClient *ssh.Client

	lhandListener net.Listener
	cancel        context.CancelFunc
	waitForMe     sync.WaitGroup
}

// Start starts a forward based on the structure specified
func (f *PortForward) Start() (string, error) {
	log.WithFields(log.Fields{
		"f": f,
	}).Debug("Starting server")
	switch f.Direction {
	case LocalToRemote:
		return f.localToRemote()
	default:
		panic("unimplemented")
	}
}

// Stop stops forwarding
func (f *PortForward) Stop() {
	log.WithFields(log.Fields{
		"f": f,
	}).Debug("Stopping server")
	f.lhandListener.Close()
	f.cancel()
}

// WaitForRemote waits until the remote side responds
func (f *PortForward) WaitForRemote() error {
	if f.lhandListener == nil {
		return fmt.Errorf("not listening")
	}
	localAddr := f.lhandListener.Addr()
	buf := make([]byte, 10)
	for {
		conn, err := net.Dial(localAddr.Network(), localAddr.String())
		if err != nil {
			return err
		}
		tConn := &gNet.TimeoutConn{
			Conn:        conn,
			IdleTimeout: time.Second * 1,
		}
		_, err = tConn.Read(buf)
		log.WithError(err).Debug("Client disconnect")
		conn.Close()

		if err == io.EOF {
			time.Sleep(time.Millisecond * 500)
		} else {
			break
		}
	}
	return nil
}

func (f *PortForward) localToRemoteSSHDial(ctx context.Context, client net.Conn) {
	defer client.Close()
	tunnel, err := f.SSHClient.Dial(f.RemoteProto, f.RemoteAddr)
	if err != nil {
		log.WithError(err).Info("Failed to dial")
		return
	}
	log.WithFields(log.Fields{
		"t": tunnel,
	}).Debug("tunnel opening")
	defer func() {
		log.WithFields(log.Fields{
			"t": tunnel,
		}).Debug("tunnel closing")
		tunnel.Close()
	}()

	gNet.PipeConn(ctx, client, tunnel)

}

func (f *PortForward) localToRemoteAcceptLoop(local net.Listener) {
	defer local.Close()
	ctx, cancel := context.WithCancel(context.Background())
	f.cancel = cancel
	for {
		client, err := local.Accept()
		if err != nil {
			log.WithError(err).Info("Failed to accept")
			return
		}
		log.WithFields(log.Fields{
			"remote": client.RemoteAddr().String(),
		}).Debug("new client forward")

		go f.localToRemoteSSHDial(ctx, client)
	}
}

func (f *PortForward) localToRemote() (string, error) {
	local, err := net.Listen(f.LocalProto, f.LocalAddr)
	if err != nil {
		return "", err
	}
	f.lhandListener = local

	// TODO: return error when forwarding prohibited
	go f.localToRemoteAcceptLoop(local)
	return local.Addr().String(), nil
}
