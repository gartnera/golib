package ssh

import (
	"os"
	"testing"

	"go.uber.org/goleak"
)

const testingSSHFixture = "registry.gitlab.com/gartnera/golib/ssh-fixture"

var testHost string

func TestMain(m *testing.M) {
	testHost = os.Getenv("TEST_HOST")
	if testHost == "" {
		testHost = "localhost"
	}

	goleak.VerifyTestMain(m)
}
