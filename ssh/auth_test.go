package ssh

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"testing"

	gDocker "gitlab.com/gartnera/golib/docker"
	"golang.org/x/crypto/ssh"
)

// TODO: manual overrides

func getSSHKey(t *testing.T) ssh.AuthMethod {
	_, filename, _, _ := runtime.Caller(0)
	dir := filepath.Dir(filename)
	keyPath := filepath.Join(dir, "fixtures", "id_rsa")

	return PublicKeyFileAuth(keyPath)
}

func TestPublicKeyFileAuth(t *testing.T) {
	t.Parallel()

	if getSSHKey(t) == nil {
		t.Error("unable to load key")
	}
}

func TestKeyCallback(t *testing.T) {
	err := gDocker.PullContainer(testingSSHFixture)
	if err != nil {
		t.Error(err)
		return
	}

	t.Run("Default", func(t *testing.T) {
		t.Parallel()
		id, ports, err := gDocker.StartContainerWithAllPorts(testingSSHFixture, false)
		defer gDocker.CleanupContainer(id)
		if err != nil {
			t.Error(err)
			return
		}
		target := fmt.Sprintf("%s:%s", testHost, ports["22"])

		file, err := ioutil.TempFile("", "hosts")
		if err != nil {
			t.Error(err)
		}
		fileName := file.Name()
		file.Close()
		defer os.Remove(fileName)

		input := strings.NewReader("yes\n")
		var output bytes.Buffer
		outputWriter := bufio.NewWriter(&output)
		cbConfig := KeyCallbackConfig{
			HostsFile: fileName,
			Input:     input,
			Output:    outputWriter,
		}

		config := &ssh.ClientConfig{
			User: "test",
			Auth: []ssh.AuthMethod{
				getSSHKey(t),
			},
			HostKeyCallback: KeyCallback(cbConfig),
		}

		client, err := ssh.Dial("tcp", target, config)
		if err != nil {
			t.Error(err)
			return
		}
		client.Close()

		outputWriter.Flush()
		line, err := output.ReadString('\n')
		if err != nil {
			t.Error(err)
			return
		}
		if !strings.HasPrefix(line, "The authenticity of host") {
			t.Error("unexpected output")
			return
		}

		info, err := os.Stat(fileName)
		if err != nil {
			t.Error(err)
			return
		}
		if info.Size() == 0 {
			t.Error("key not written to file")
		}
	})

	t.Run("IgnoreHostKey", func(t *testing.T) {
		t.Parallel()
		id, ports, err := gDocker.StartContainerWithAllPorts(testingSSHFixture, false)
		defer gDocker.CleanupContainer(id)
		if err != nil {
			t.Error(err)
			return
		}
		target := fmt.Sprintf("%s:%s", testHost, ports["22"])

		cbConfig := KeyCallbackConfig{
			IgnoreHostKey: true,
		}

		config := &ssh.ClientConfig{
			User: "test",
			Auth: []ssh.AuthMethod{
				getSSHKey(t),
			},
			HostKeyCallback: KeyCallback(cbConfig),
		}

		client, err := ssh.Dial("tcp", target, config)
		if err != nil {
			t.Error(err)
			return
		}
		client.Close()
	})
	t.Run("AutoTrust", func(t *testing.T) {
		t.Parallel()
		id, ports, err := gDocker.StartContainerWithAllPorts(testingSSHFixture, false)
		defer gDocker.CleanupContainer(id)
		if err != nil {
			t.Error(err)
			return
		}
		target := fmt.Sprintf("%s:%s", testHost, ports["22"])

		file, err := ioutil.TempFile("", "hosts")
		if err != nil {
			t.Error(err)
			return
		}
		fileName := file.Name()
		file.Close()
		defer os.Remove(fileName)

		cbConfig := KeyCallbackConfig{
			HostsFile:       fileName,
			ShouldAutoTrust: true,
		}

		config := &ssh.ClientConfig{
			User: "test",
			Auth: []ssh.AuthMethod{
				getSSHKey(t),
			},
			HostKeyCallback: KeyCallback(cbConfig),
		}

		client, err := ssh.Dial("tcp", target, config)
		if err != nil {
			t.Error(err)
			return
		}
		client.Close()

		info, err := os.Stat(fileName)
		if err != nil {
			t.Error(err)
		}
		if info.Size() == 0 {
			t.Error("key not written to file")
		}
	})

	t.Run("SkipTrust", func(t *testing.T) {
		t.Parallel()
		id, ports, err := gDocker.StartContainerWithAllPorts(testingSSHFixture, false)
		defer gDocker.CleanupContainer(id)
		if err != nil {
			t.Error(err)
			return
		}
		target := fmt.Sprintf("%s:%s", testHost, ports["22"])

		file, err := ioutil.TempFile("", "hosts")
		if err != nil {
			t.Error(err)
		}
		fileName := file.Name()
		file.Close()

		// Remove the file to test creation
		os.Remove(fileName)
		defer os.Remove(fileName)

		cbConfig := KeyCallbackConfig{
			HostsFile:       fileName,
			ShouldAutoTrust: true,
			SkipTrust:       true,
		}

		config := &ssh.ClientConfig{
			User: "test",
			Auth: []ssh.AuthMethod{
				getSSHKey(t),
			},
			HostKeyCallback: KeyCallback(cbConfig),
		}

		client, err := ssh.Dial("tcp", target, config)
		if err != nil {
			t.Error(err)
			return
		}
		client.Close()

		info, err := os.Stat(fileName)
		if err != nil {
			t.Error(err)
			return
		}
		if info.Size() != 0 {
			t.Error("key written to file")
		}
	})
}
