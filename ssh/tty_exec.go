package ssh

import (
	"os"

	"golang.org/x/crypto/ssh"
)

// DefaultTTYMode is the default TTY settings from the docs
var DefaultTTYMode = ssh.TerminalModes{
	ssh.ECHO:          0,     // disable echoing
	ssh.TTY_OP_ISPEED: 14400, // input speed = 14.4kbaud
	ssh.TTY_OP_OSPEED: 14400, // output speed = 14.4kbaud
}

// StartInPTY starts a cmd in a PTY via an SSH client
//
// This is useful because if you execute a command without pty, it won't
// automatically be stopped when the ssh client disconnects.
func StartInPTY(client *ssh.Client, cmd string) (*ssh.Session, error) {
	session, err := client.NewSession()
	if err != nil {
		return nil, err
	}
	session.Stdout = os.Stdout
	session.Stderr = os.Stderr
	err = session.RequestPty("xterm", 80, 80, DefaultTTYMode)
	if err != nil {
		return nil, err
	}
	err = session.Start(cmd)
	if err != nil {
		return nil, err
	}
	return session, nil
}
