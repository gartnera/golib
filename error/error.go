package error

// PanicError Panics if err != nil
func PanicError(err error) {
	if err != nil {
		panic(err)
	}
}
