package log

import (
	"strings"
	"encoding/json"

	"github.com/sirupsen/logrus"
	"github.com/ssgreg/journald"
)

// TextFormatter adds formatting for nested logrus.Fields
type TextFormatter struct {
	logrus.TextFormatter
	disableFlatten bool
}

func formatFields(e Fields, sep string, formatKeys bool, disableFlatten bool) Fields {
	newFields := Fields{}
	for key, value := range e {
		if formatKeys {
			key = strings.ToUpper(key)
		}
		switch fValue := value.(type) {
		case Fields:
			if disableFlatten {
				j, _ := json.Marshal(fValue)
				newFields[key] = string(j)
				continue
			}
			for ffKey, ffValue := range fValue {
				nKey := key + sep + ffKey
				if formatKeys {
					nKey = strings.ToUpper(nKey)
				}
				newFields[nKey] = ffValue
			}
		default:
			newFields[key] = value
		}
	}
	return newFields
}

// Format flattens nested Entries and call logrus.TextFormatter
func (f *TextFormatter) Format(e *Entry) ([]byte, error) {
	e.Data = formatFields(e.Data, ".", false, f.disableFlatten)
	return f.TextFormatter.Format(e)
}

// JournaldHook to send logs via Journald.
type JournaldHook struct{
	disableFlatten bool
}

// Fire maps logrus priority to journald priority and writes message
func (h *JournaldHook) Fire(entry *logrus.Entry) error {
	level := entry.Level
	var priority journald.Priority
	switch level {
	case PanicLevel:
		priority = journald.PriorityEmerg
	case FatalLevel:
		priority = journald.PriorityAlert
	case ErrorLevel:
		priority = journald.PriorityErr
	case WarnLevel:
		priority = journald.PriorityWarning
	case InfoLevel:
		priority = journald.PriorityInfo
	case DebugLevel, TraceLevel:
		priority = journald.PriorityDebug
	}

	data := formatFields(entry.Data, "_", true, h.disableFlatten)
	return journald.Send(entry.Message, priority, data)
}

// Levels lists levels to Fire during
func (h *JournaldHook) Levels() []Level {
	return AllLevels
}
