package log

import (
	"bufio"
	"net"
	"os"
	"path/filepath"
	"strconv"
)

var socket net.Listener

// StartLevelSocket starts a log level control socket at /tmp/log-level-ctl/<name>.<pid>
func StartLevelSocket() error {
	name := filepath.Base(os.Args[0])
	pid := strconv.Itoa(os.Getpid())

	socketName := name + "." + pid
	dir := "/tmp/log-level-ctl/"
	os.Mkdir(dir, 01777)
	path := dir + socketName

	return StartNamedLevelSocket(path)
}

// StartNamedLevelSocket starts a log level control socket at path
func StartNamedLevelSocket(path string) error {
	var err error

	os.Remove(path)

	socket, err = net.Listen("unix", path)
	if err != nil {
		return err
	}

	go func() {
		defer socket.Close()
		for {
			conn, err := socket.Accept()
			if err != nil {
				return
			}

			reader := bufio.NewReader(conn)
			line, _, err := reader.ReadLine()
			if err == nil {
				s := string(line)
				lvl, err := ParseLevel(s)
				if err != nil {
					conn.Write([]byte("Error: " + err.Error() + "\n"))
				} else {
					SetLevel(lvl)
					conn.Write([]byte("OK\n"))
				}
			}
			conn.Close()
		}
	}()

	return nil
}

// StopLevelSocket stop the log level control if it exists
func StopLevelSocket() {
	if socket != nil {
		socket.Close()
	}
}
