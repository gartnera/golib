package log

import (
	"reflect"
	"unsafe"
)

// WithStruct converts a struct with `llevel` tags to logrus.Fields
func WithStruct(s interface{}) *Entry {
	return WithPrefixedStruct("", s)
}

// WithPrefixedStruct adds prefix to each WithStruct
// Log key will be <name>.<field-name>
func WithPrefixedStruct(prefix string, s interface{}) *Entry {
	fields := Fields{}

	std := StandardLogger()
	lvl := std.GetLevel()

	t := reflect.TypeOf(s)
	v := reflect.ValueOf(s)

	var m interface{}
	var ok bool
	if prefix != "" {
		m, ok = fields[prefix]
		if !ok {
			m = make(Fields)
			fields[prefix] = m
		}
	} else {
		m = fields
	}

	tName := t.Name()
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		// Get the field tag value
		tag := field.Tag.Get("llevel")
		if tag == "" {
			continue
		}
		fieldLevel, err := ParseLevel(tag)
		if err != nil {
			std.Fatalf("Failed to parse llevel on type %s (got %s)\n", tName, tag)
		}

		if fieldLevel <= lvl {
			// bypass internal only fields
			rs2 := reflect.New(v.Type()).Elem()
			rs2.Set(v)
			value := rs2.Field(i)
			value = reflect.NewAt(value.Type(), unsafe.Pointer(value.UnsafeAddr())).Elem()

			m2 := m.(Fields)
			m2[field.Name] = value.Interface()
		}

	}

	return WithFields(fields)
}
