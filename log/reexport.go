package log

import (
	"github.com/sirupsen/logrus"
)

// StandardLogger returns std
var StandardLogger = logrus.StandardLogger

// SetOutput sets the standard logger output.
var SetOutput = logrus.SetOutput

// SetFormatter sets the standard logger formatter.
var SetFormatter = logrus.SetFormatter

// SetReportCaller sets whether the standard logger will include the calling
// method as a field.
var SetReportCaller = logrus.SetReportCaller

// ParseLevel takes a string level and returns the Logrus log level constant.
var ParseLevel = logrus.ParseLevel

// SetLevel sets the standard logger level.
var SetLevel = logrus.SetLevel

// GetLevel returns the standard logger level.
var GetLevel = logrus.GetLevel

// IsLevelEnabled checks if the log level of the standard logger is greater than the level param
var IsLevelEnabled = logrus.IsLevelEnabled

// AddHook adds a hook to the standard logger hooks.
var AddHook = logrus.AddHook

// WithError creates an entry from the standard logger and adds an error to it, using the value defined in ErrorKey as key.
var WithError = logrus.WithError

// WithContext creates an entry from the standard logger and adds a context to it.
var WithContext = logrus.WithContext

// WithField creates an entry from the standard logger and adds a field to
// it. If you want multiple fields, use `WithFields`.
//
// Note that it doesn't log until you call Debug, Print, Info, Warn, Fatal
// or Panic on the Entry it returns.
var WithField = logrus.WithField

// WithFields creates an entry from the standard logger and adds multiple
// fields to it. This is simply a helper for `WithField`, invoking it
// once for each field.
//
// Note that it doesn't log until you call Debug, Print, Info, Warn, Fatal
// or Panic on the Entry it returns.
var WithFields = logrus.WithFields

// WithTime creats an entry from the standard logger and overrides the time of
// logs generated with it.
//
// Note that it doesn't log until you call Debug, Print, Info, Warn, Fatal
// or Panic on the Entry it returns.
var WithTime = logrus.WithTime

// Trace logs a message at level Trace on the standard logger.
var Trace = logrus.Trace

// Debug logs a message at level Debug on the standard logger.
var Debug = logrus.Debug

// Print logs a message at level Info on the standard logger.
var Print = logrus.Print

// Info logs a message at level Info on the standard logger.
var Info = logrus.Info

// Warn logs a message at level Warn on the standard logger.
var Warn = logrus.Warn

// Warning logs a message at level Warn on the standard logger.
var Warning = logrus.Warning

// Error logs a message at level Error on the standard logger.
var Error = logrus.Error

// Panic logs a message at level Panic on the standard logger.
var Panic = logrus.Panic

// Fatal logs a message at level Fatal on the standard logger then the process will exit with status set to 1.
var Fatal = logrus.Fatal

// Tracef logs a message at level Trace on the standard logger.
var Tracef = logrus.Tracef

// Debugf logs a message at level Debug on the standard logger.
var Debugf = logrus.Debugf

// Printf logs a message at level Info on the standard logger.
var Printf = logrus.Printf

// Infof logs a message at level Info on the standard logger.
var Infof = logrus.Infof

// Warnf logs a message at level Warn on the standard logger.
var Warnf = logrus.Warnf

// Warningf logs a message at level Warn on the standard logger.
var Warningf = logrus.Warningf

// Errorf logs a message at level Error on the standard logger.
var Errorf = logrus.Errorf

// Panicf logs a message at level Panic on the standard logger.
var Panicf = logrus.Panicf

// Fatalf logs a message at level Fatal on the standard logger then the process will exit with status set to 1.
var Fatalf = logrus.Fatalf

// Traceln logs a message at level Trace on the standard logger.
var Traceln = logrus.Traceln

// Debugln logs a message at level Debug on the standard logger.
var Debugln = logrus.Debugln

// Println logs a message at level Info on the standard logger.
var Println = logrus.Println

// Infoln logs a message at level Info on the standard logger.
var Infoln = logrus.Infoln

// Warnln logs a message at level Warn on the standard logger.
var Warnln = logrus.Warnln

// Warningln logs a message at level Warn on the standard logger.
var Warningln = logrus.Warningln

// Errorln logs a message at level Error on the standard logger.
var Errorln = logrus.Errorln

// Panicln logs a message at level Panic on the standard logger.
var Panicln = logrus.Panicln

// Fatalln logs a message at level Fatal on the standard logger then the process will exit with status set to 1.
var Fatalln = logrus.Fatalln

// Entry is the final or intermediate Logrus logging entry. It contains all the fields passed with WithField{,s}. It's finally logged when Trace, Debug, Info, Warn, Error, Fatal or Panic is called on it. These objects can be reused and passed around as much as you wish to avoid field duplication.
type Entry = logrus.Entry

// Fields used to pass to `WithFields`.
type Fields = logrus.Fields

type Level = logrus.Level

// AllLevels A constant exposing all logging levels
var AllLevels = []logrus.Level{
	PanicLevel,
	FatalLevel,
	ErrorLevel,
	WarnLevel,
	InfoLevel,
	DebugLevel,
	TraceLevel,
}

// These are the different logging levels. You can set the logging level to log
// on your instance of logger, obtained with `logrus.New()`.
const (
	// PanicLevel level, highest level of severity. Logs and then calls panic with the
	// message passed to Debug, Info, ...
	PanicLevel = logrus.PanicLevel
	// FatalLevel level. Logs and then calls `logger.Exit(1)`. It will exit even if the
	// logging level is set to Panic.
	FatalLevel = logrus.FatalLevel
	// ErrorLevel level. Logs. Used for errors that should definitely be noted.
	// Commonly used for hooks to send errors to an error tracking service.
	ErrorLevel = logrus.ErrorLevel
	// WarnLevel level. Non-critical entries that deserve eyes.
	WarnLevel = logrus.WarnLevel
	// InfoLevel level. General operational entries about what's going on inside the
	// application.
	InfoLevel = logrus.InfoLevel
	// DebugLevel level. Usually only enabled when debugging. Very verbose logging.
	DebugLevel = logrus.DebugLevel
	// TraceLevel level. Designates finer-grained informational events than the Debug.
	TraceLevel = logrus.TraceLevel
)
