package log

import (
	"testing"
)

type testingStruct struct {
	one   string            `llevel:"info"`
	two   int64             `llevel:"warn"`
	three *string           `llevel:"error"`
	four  map[string]string `llevel:"error"`
	five  string
}

var t1 = testingStruct{
	one: "asdf",
	four: map[string]string{
		"asdf": "asdf",
	},
}

func Test_WithStruct(t *testing.T) {
	a := "asdf"
	t1.three = &a
	WithStruct(t1).Warn("")
}

func Test_WithPrefixedStruct(t *testing.T) {
	a := "asdf"
	t1.three = &a
	WithPrefixedStruct("t1", t1).Warn("")
}
