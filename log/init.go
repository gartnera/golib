package log

import (
	"io/ioutil"
	"os"

	"github.com/sirupsen/logrus"
)

func init() {
	lvlStr := os.Getenv("LOG_LEVEL")
	lvl, err := ParseLevel(lvlStr)
	if err != nil {
		lvl = WarnLevel
	}
	SetLevel(lvl)

	_, isSystemd := os.LookupEnv("INVOCATION_ID")
	_, disableFlatten := os.LookupEnv("LOG_DISABLE_FLATTEN")
	_, shouldReportCaller := os.LookupEnv("LOG_LOOKUP_CALLER")
	if shouldReportCaller {
		SetReportCaller(true)
	}

	fmtStr := os.Getenv("LOG_FORMAT")
	if (fmtStr == "" && isSystemd) || fmtStr == "journald" {
		AddHook(&JournaldHook{
			disableFlatten: disableFlatten,
		})
		logrus.SetOutput(ioutil.Discard)
	} else if fmtStr == "json" {
		SetFormatter(&logrus.JSONFormatter{})
	} else {
		SetFormatter(&TextFormatter{
			disableFlatten: disableFlatten,
		})
	}
}
