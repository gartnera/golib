package log

import (
	"bufio"
	"io"
)

// ReaderToDebug reads from an io.Reader and writes to a logger
func ReaderToDebug(r io.Reader) {
	rBuf := bufio.NewReader(r)
	for {
		line, _, err := rBuf.ReadLine()
		if err != nil {
			return
		}
		Debug(string(line))
	}

}
