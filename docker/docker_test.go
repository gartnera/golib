package docker

import (
	"testing"

	"github.com/docker/docker/client"
	"go.uber.org/goleak"
	"golang.org/x/net/context"
)

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}

func TestStartAndCleanup(t *testing.T) {
	id, _, err := StartContainerWithAllPorts("registry.gitlab.com/gartnera/golib/ssh-fixture", true)
	if err != nil {
		t.Error(err)
	}
	CleanupContainer(id)

	cli, err := client.NewEnvClient()
	if err != nil {
		t.Error(err)
	}
	defer cli.Close()

	_, err = cli.ContainerInspect(context.Background(), id)
	if err == nil {
		t.Error("container not deleted")
	}
}
