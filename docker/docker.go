package docker

import (
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	"golang.org/x/net/context"

	log "gitlab.com/gartnera/golib/log"
)

// FlattenPortMap flattens `nat.PortMap` to a simple msp
func FlattenPortMap(set nat.PortMap) map[string]string {
	res := make(map[string]string)

	for key, value := range set {
		res[key.Port()] = value[0].HostPort
	}
	return res
}

// PullContainer pulls a docker container
func PullContainer(imageName string) error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	cli, err := client.NewEnvClient()
	if err != nil {
		return err
	}
	defer cli.Close()

	out, err := cli.ImagePull(ctx, imageName, types.ImagePullOptions{})
	if err != nil {
		return err
	}
	log.ReaderToDebug(out)
	out.Close()
	return nil
}

// StartContainerWithAllPorts start a container with image `imageName` listening
//
// The map returned is a map of the exposed port the the current host port
func StartContainerWithAllPorts(imageName string, pull bool) (string, map[string]string, error) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	cli, err := client.NewEnvClient()
	if err != nil {
		return "", nil, err
	}
	defer cli.Close()

	if pull {
		err := PullContainer(imageName)
		if err != nil {
			return "", nil, err
		}
	}

	containerConfig := &container.Config{
		Image: imageName,
	}

	hostConfig := &container.HostConfig{
		PublishAllPorts: true,
	}

	resp, err := cli.ContainerCreate(ctx, containerConfig, hostConfig, nil, "")
	if err != nil {
		return "", nil, err
	}

	id := resp.ID

	if err := cli.ContainerStart(ctx, id, types.ContainerStartOptions{}); err != nil {
		return "", nil, err
	}

	info, err := cli.ContainerInspect(ctx, id)
	if err != nil {
		return "", nil, err
	}

	ports := info.NetworkSettings.Ports
	flatPorts := FlattenPortMap(ports)
	return id, flatPorts, err
}

// CleanupContainer stops and removes a container by ID
func CleanupContainer(id string) error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	cli, err := client.NewEnvClient()
	if err != nil {
		return err
	}
	defer cli.Close()

	// Ignore if container was already removed etc
	cli.ContainerRemove(ctx, id, types.ContainerRemoveOptions{
		Force: true,
	})

	return nil
}
