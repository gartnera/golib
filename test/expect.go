package test

import (
	"testing"
)

// Expect expects a == b
func Expect(t *testing.T, a string, b string) {
	if a != b {
		t.Helper()
		t.Errorf("expected %q got %q", a, b)
	}
}
