package test

import "testing"

// Error signals test failure if err != nil
func Error(t *testing.T, err error) {
	t.Helper()
	ErrorMsg(t, err, "")
}

// ErrorMsg signals test failure if err != nil (with msg)
func ErrorMsg(t *testing.T, err error, msg string) {
	t.Helper()
	if err != nil {
		t.Error(msg, err)
	}
}
