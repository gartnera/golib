package net

import (
	"net"
	"time"
)

// TimeoutConn adds timeout's to an existing conn
type TimeoutConn struct {
	net.Conn
	IdleTimeout time.Duration
}

func (c *TimeoutConn) Write(p []byte) (int, error) {
	c.updateDeadline()
	return c.Conn.Write(p)
}

func (c *TimeoutConn) Read(b []byte) (int, error) {
	c.updateDeadline()
	return c.Conn.Read(b)
}

func (c *TimeoutConn) updateDeadline() {
	idleDeadline := time.Now().Add(c.IdleTimeout)
	c.Conn.SetDeadline(idleDeadline)
}
