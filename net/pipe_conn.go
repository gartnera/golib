package net

// https://github.com/sgreben/sshtunnel/blob/master/connpipe/connpipe.go
// MIT License Copyright (c) 2018 Sergey Grebenshchikov

import (
	"context"
	"net"

	gIO "gitlab.com/gartnera/golib/io"
)

// PipeConn starts a two-way copy between the two connections.
func PipeConn(ctx context.Context, a net.Conn, b net.Conn) {
	go gIO.CopyCtx(ctx, a, b)
	gIO.CopyCtx(ctx, b, a)
}
