package file

import (
	"os"
	"os/user"
	"path/filepath"
	"strings"

	log "gitlab.com/gartnera/golib/log"
)

// Exists Checks if a file exists
func Exists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

// The following function is Copyright 2014 The fleet Authors
// Licensed under the Apache License, Version 2.0 (the "License");

// ParseFilepath expands ~ and ~user constructions.
// If user or $HOME is unknown, do nothing.
func ParseFilepath(path string) string {
	if !strings.HasPrefix(path, "~") {
		return path
	}
	i := strings.Index(path, "/")
	if i < 0 {
		i = len(path)
	}
	var home string
	if i == 1 {
		if home = os.Getenv("HOME"); home == "" {
			usr, err := user.Current()
			if err != nil {
				log.WithError(err).Debug("Failed to get current home directory")
				return path
			}
			home = usr.HomeDir
		}
	} else {
		usr, err := user.Lookup(path[1:i])
		if err != nil {
			log.WithFields(log.Fields{
				"user": path[1:i],
			}).WithError(err).Debug("Failed to get home directory")
			return path
		}
		home = usr.HomeDir
	}
	path = filepath.Join(home, path[i:])
	return path
}

// Append appends data to a file
func Append(file string, data string) error {
	// If the file doesn't exist, create it, or append to the file
	f, err := os.OpenFile(file, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()
	if _, err := f.Write([]byte(data)); err != nil {
		return err
	}
	return nil
}
