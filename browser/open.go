// Package chrome Utilities to open chrome
package chrome

// https://github.com/cdr/sshcode/blob/master/main.go
// MIT License Copyright 2019 Coder Technologies, Inc.

import (
	"os"
	"os/exec"

	"github.com/pkg/browser"
	log "gitlab.com/gartnera/golib/log"
)

// OpenChrome try's to open chrome
func OpenChrome(url string, asApp bool, incognito bool, disableStuff bool) {
	var openCmd *exec.Cmd

	const (
		macPath = "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome"
		wslPath = "/mnt/c/Program Files (x86)/Google/Chrome/Application/chrome.exe"
	)

	var options []string
	if disableStuff {
		options = append(options, "--disable-extensions", "--disable-plugins")
	}

	if incognito {
		options = append(options, "--incognito")
	}

	if asApp {
		options = append(options, "--app="+url)
	} else {
		options = append(options, url)
	}

	switch {
	case commandExists("google-chrome"):
		openCmd = exec.Command("google-chrome", options...)
	case commandExists("google-chrome-stable"):
		openCmd = exec.Command("google-chrome-stable", options...)
	case commandExists("chromium"):
		openCmd = exec.Command("chromium", options...)
	case commandExists("chromium-browser"):
		openCmd = exec.Command("chromium-browser", options...)
	case pathExists(macPath):
		openCmd = exec.Command(macPath, options...)
	case pathExists(wslPath):
		openCmd = exec.Command(wslPath, options...)
	default:
		err := browser.OpenURL(url)
		if err != nil {
			log.WithError(err).Error("Failed to open browser")
		}
		return
	}

	// We do not use CombinedOutput because if there is no chrome instance, this will block
	// and become the parent process instead of using an existing chrome instance.
	err := openCmd.Start()
	if err != nil {
		log.WithError(err).Error("Failed to open browser")
	}
}

// Checks if a command exists locally.
func commandExists(name string) bool {
	_, err := exec.LookPath(name)
	return err == nil
}

func pathExists(name string) bool {
	_, err := os.Stat(name)
	return err == nil
}
